/*
 * Name: altera_pci_module.c
 *
 * Kernel-side driver for the
 * ALTERA PCIE evaluation boards
 * DK-DEV-4SGX230N
 *
 * $Id$
 *
 */

#include <linux/version.h>

#include <linux/types.h>
#include <linux/pci.h>
#include <linux/module.h>
#include <linux/interrupt.h>
#include <linux/poll.h>

#if 0

/* Copy pgprot_noncached() from .../drivers/char/mem.c */

#ifndef pgprot_noncached

/*
 * This should probably be per-architecture in <asm/pgtable.h>
 */
static inline pgprot_t pgprot_noncached(pgprot_t _prot)
{
	unsigned long prot = pgprot_val(_prot);

#if defined(__i386__)
	/* On PPro and successors, PCD alone doesn't always mean 
	    uncached because of interactions with the MTRRs. PCD | PWT
	    means definitely uncached. */ 
	if (boot_cpu_data.x86 > 3)
		prot |= _PAGE_PCD | _PAGE_PWT;
#elif defined(__powerpc__)
	prot |= _PAGE_NO_CACHE | _PAGE_GUARDED;
#elif defined(__mc68000__)
#ifdef SUN3_PAGE_NOCACHE
	if (MMU_IS_SUN3)
		prot |= SUN3_PAGE_NOCACHE;
	else
#endif
	if (MMU_IS_851 || MMU_IS_030)
		prot |= _PAGE_NOCACHE030;
	/* Use no-cache mode, serialized */
	else if (MMU_IS_040 || MMU_IS_060)
		prot = (prot & _CACHEMASK040) | _PAGE_NOCACHE_S;
#elif defined(__mips__)
	prot = (prot & ~_CACHE_MASK) | _CACHE_UNCACHED;
#elif defined(__arm__) && defined(CONFIG_CPU_32)
	/* Turn off caching for all I/O areas */
	prot &= ~(L_PTE_CACHEABLE | L_PTE_BUFFERABLE);
#endif

	return __pgprot(prot);
}

#endif /* !pgprot_noncached */

#endif

static struct pci_dev* udev_pdev = NULL;
static int             udev_pfn = 0;
static char*           udev_regs = 0;

static u32 udev_reg0x10 = 0;
static u32 udev_reg0x14 = 0;


#ifdef HAVE_INTR
static u8    udev_irq = 0;
static int udev_intr_count = 0;
#endif


static int regs_open(struct inode * inode, struct file * file)
{
  //printk(KERN_INFO MOD_NAME "::regs_open: Here!\n");
  
  if (1) {
     u32 v;
     pci_read_config_dword(udev_pdev, 0x10, &v);

     if ((v & 0xF0000000) == 0) {
        printk(KERN_INFO MOD_NAME "::regs_open Config space [0x%02x] : 0x%08x, FPGA was probably reloaded\n", 0x10, v);
        
        if (1) {
           printk(KERN_INFO MOD_NAME ": Restoring PCI Config space\n");
           pci_write_config_dword(udev_pdev, 0x04, 0x00100006);
           pci_write_config_dword(udev_pdev, 0x10, udev_reg0x10);
           pci_write_config_dword(udev_pdev, 0x14, udev_reg0x14);
        }
     }
  }

  return 0;
}

static int regs_release(struct inode * inode, struct file * file)
{
  //printk(KERN_INFO MOD_NAME "::regs_release: Here!\n");
  return 0;
}

static int regs_mmap(struct file *file,struct vm_area_struct *vma)
{
  int err;

  /* Only allow mmap() with these arguments */
  const u32 enforce_size = 0x100000;

  /* mmap() arguments */
  u32 offset = vma->vm_pgoff << PAGE_SHIFT;
  u32 size   = vma->vm_end - vma->vm_start;

  if (0)
    {
      printk(KERN_INFO MOD_NAME "::regs_mmap: start: 0x%08lx, end: 0x%08lx, vm_pgoff: 0x%lx, offset: 0x%x, size: 0x%x\n",
	     vma->vm_start,
	     vma->vm_end,
	     vma->vm_pgoff,
	     offset,
	     size);
    }

  if (size != enforce_size)
    {
      printk(KERN_ERR MOD_NAME "::regs_mmap: Error: Attempt to mmap PCI registers with map size 0x%x != 0x%x\n",size,enforce_size);
      return -EINVAL;
    }

  /* MMIO pages should be non-cached */
  //vma->vm_page_prot = pgprot_noncached(vma->vm_page_prot);
  //vma->vm_page_prot = (vma->vm_page_prot);

  /* Don't try to swap out physical pages.. */
  vma->vm_flags |= VM_RESERVED;

  /* Don't dump addresses that are not real memory to a core file. */
  //vma->vm_flags |= VM_IO;

  err = remap_pfn_range(vma, vma->vm_start, udev_pfn, vma->vm_end - vma->vm_start, vma->vm_page_prot);
  if (err)
    {
      printk(KERN_ERR MOD_NAME "::regs_mmap: remap_pfn_range (regs) err %d\n", err);
      return -EIO;
    }

  return 0;
}

static struct file_operations regs_fops = {
  mmap:		regs_mmap,
  open:		regs_open,
  release:	regs_release
};

#ifdef HAVE_INTR
static int intr_handler(int irq, void *dev_id, struct pt_regs *regs)
{
  int mine;
  u32 stat, statr;

#if 0
  stat = ioread32(udev_softdac_regs + 0x80010);
  mine = stat & 0x70000000;
  if (mine)
    {
      //plx9080_intr_show();
      iowrite8(0x70, udev_softdac_regs + 0x80013);
      // issue a PCI read to flush posted PCI writes
      statr = ioread32(udev_softdac_regs + 0x80010);
      printk(KERN_INFO MOD_NAME ": interrupt irq %d, stat 0x%08x -> 0x%08x, count %d\n", irq, stat, statr, udev_intr_count++);
      //plx9080_intr_show();
      return IRQ_HANDLED;
    }
#endif

  printk(KERN_INFO MOD_NAME ": (for somebody else...) interrupt irq %d, stat 0x%08x, count %d\n", irq, stat, udev_intr_count++);

  return IRQ_NONE;
}
#endif

static int init(struct pci_dev *pdev)
{
  printk(KERN_INFO MOD_NAME ": Device \'%s\' pdev 0x%p\n", pci_name(pdev), pdev);

  udev_pdev = pdev;

  /* Enable PCI device */
  pci_enable_device(pdev);

  /* Enable PCI bus-master */
  pci_set_master(pdev);

  if (! (pci_resource_flags(pdev,0) & IORESOURCE_MEM))
    {
      printk(KERN_ERR MOD_NAME ": Error: BAR0 is not MMIO (memory mapped I/O address space)\n");
      return -ENODEV;
    }

  if (1)
     {
        unsigned long start = pci_resource_start(pdev,0);
        unsigned long len  = pci_resource_len(pdev,0);

        if (!request_mem_region(start, len, MOD_NAME))
           {
              printk (KERN_ERR MOD_NAME ": Error: Cannot allocate a PCI MMIO BAR0 region at 0x%lx, length 0x%lx\n", start, len);
              return -ENODEV;
           }
        
        udev_pfn = start >> PAGE_SHIFT;
        //udev_regs = ioremap_nocache(start, len);
        udev_regs = ioremap(start, len);
        
        printk(KERN_INFO MOD_NAME ": PCI BAR0 at 0x%lx mapped to local addr 0x%p, length 0x%lx, pfn 0x%x\n", start, udev_regs, len, udev_pfn);
     }
  
  if (0)
     {
        int i;
        for (i=0; i<=0x10; i+=4)
           {
              int w = ioread32(udev_regs + i);
              printk(KERN_INFO MOD_NAME ": register 0x%02x: 0x%08x\n", i, w);
           }
     }
        
  if (0)
    {
      int i;
      for (i=0; i<=0x10; i+=4)
	{
           iowrite32(0xdeadbeef, udev_regs + i);
           ioread32(udev_regs + i);
	}
    }

#ifdef HAVE_INTR
  udev_irq = pdev->irq;
  printk(KERN_INFO MOD_NAME ": Using irq %d\n", udev_irq);

  /* Connect interrupts */
  if (udev_irq > 0)
    {
      int err;
      /* we may use shared interrupts */
      err = request_irq(udev_irq, intr_handler, SA_INTERRUPT|SA_SHIRQ, MOD_NAME, udev_pdev);
      if (err)
	{
	  printk(KERN_ERR MOD_NAME ": Cannot get irq %d, err %d\n", udev_irq, err);
	  udev_irq = 0;
	}

      enable_irq(udev_irq);

      // This enables the PMC-SOFTDAC-M interrupts -
      // commented out because this driver does not
      // do anything with them - we should only enable
      // interrupts when a user program is ready to do
      // something with them. We should call plx9080_intr_enable()
      // in the open() call, or in the mmap() call. or in a
      // "wait for interrupt" call.
      //plx9080_intr_enable();
    }
#endif

  if (register_chrdev(MOD_MAJOR, MOD_NAME, &regs_fops)) {
    printk(KERN_ERR MOD_NAME ": Cannot register device\n");
    return -ENODEV;
  }

  if (1) {

     // normal values...

     /* Jun 28 15:15:16 daqtmp3 kernel: pcieadc: Config space [0x00] : 0x00041172 */
     /* Jun 28 15:15:16 daqtmp3 kernel: pcieadc: Config space [0x04] : 0x00100006 */
     /* Jun 28 15:15:16 daqtmp3 kernel: pcieadc: Config space [0x08] : 0x11000001 */
     /* Jun 28 15:15:16 daqtmp3 kernel: pcieadc: Config space [0x0c] : 0x00000000 */
     /* Jun 28 15:15:16 daqtmp3 kernel: pcieadc: Config space [0x10] : 0x9000000c */
     /* Jun 28 15:15:16 daqtmp3 kernel: pcieadc: Config space [0x14] : 0x00000000 */
     /* Jun 28 15:15:16 daqtmp3 kernel: pcieadc: Config space [0x18] : 0x00000000 */
     /* Jun 28 15:15:16 daqtmp3 kernel: pcieadc: Config space [0x1c] : 0x00000000 */
     /* Jun 28 15:15:16 daqtmp3 kernel: pcieadc: Config space [0x20] : 0x00000000 */
     /* Jun 28 15:15:16 daqtmp3 kernel: pcieadc: Config space [0x24] : 0x00000000 */
     /* Jun 28 15:15:16 daqtmp3 kernel: pcieadc: Config space [0x28] : 0x00000000 */
     /* Jun 28 15:15:16 daqtmp3 kernel: pcieadc: Config space [0x2c] : 0x00041172 */
     /* Jun 28 15:15:16 daqtmp3 kernel: pcieadc: Config space [0x30] : 0x00000000 */
     /* Jun 28 15:15:16 daqtmp3 kernel: pcieadc: Config space [0x34] : 0x00000050 */
     /* Jun 28 15:15:16 daqtmp3 kernel: pcieadc: Config space [0x38] : 0x00000000 */
     /* Jun 28 15:15:16 daqtmp3 kernel: pcieadc: Config space [0x3c] : 0x00000100 */

     // after FPGA reload...

     /* Jun 28 16:02:37 daqtmp3 kernel: pcieadc: Config space [0x00] : 0x00041172 */
     /* Jun 28 16:02:37 daqtmp3 kernel: pcieadc: Config space [0x04] : 0x00100004 */
     /* Jun 28 16:02:37 daqtmp3 kernel: pcieadc: Config space [0x08] : 0x11000001 */
     /* Jun 28 16:02:37 daqtmp3 kernel: pcieadc: Config space [0x0c] : 0x00000000 */
     /* Jun 28 16:02:37 daqtmp3 kernel: pcieadc: Config space [0x10] : 0x0000000c */
     /* Jun 28 16:02:37 daqtmp3 kernel: pcieadc: Config space [0x14] : 0x00000000 */
     /* Jun 28 16:02:37 daqtmp3 kernel: pcieadc: Config space [0x18] : 0x00000000 */
     /* Jun 28 16:02:37 daqtmp3 kernel: pcieadc: Config space [0x1c] : 0x00000000 */
     /* Jun 28 16:02:37 daqtmp3 kernel: pcieadc: Config space [0x20] : 0x00000000 */
     /* Jun 28 16:02:37 daqtmp3 kernel: pcieadc: Config space [0x24] : 0x00000000 */
     /* Jun 28 16:02:37 daqtmp3 kernel: pcieadc: Config space [0x28] : 0x00000000 */
     /* Jun 28 16:02:37 daqtmp3 kernel: pcieadc: Config space [0x2c] : 0x00041172 */
     /* Jun 28 16:02:37 daqtmp3 kernel: pcieadc: Config space [0x30] : 0x00000000 */
     /* Jun 28 16:02:37 daqtmp3 kernel: pcieadc: Config space [0x34] : 0x00000050 */
     /* Jun 28 16:02:37 daqtmp3 kernel: pcieadc: Config space [0x38] : 0x00000000 */
     /* Jun 28 16:02:37 daqtmp3 kernel: pcieadc: Config space [0x3c] : 0x00000100 */

     int i;
     for (i=0; i<0x40; i+=4) {
        u32 v;
        pci_read_config_dword(udev_pdev, i, &v);
        printk(KERN_INFO MOD_NAME ": Config space [0x%02x] : 0x%08x\n", i, v);
     }

     if (1) {
        u32 v;
        pci_read_config_dword(udev_pdev, 0x10, &v);
        udev_reg0x10 = v;

        pci_read_config_dword(udev_pdev, 0x14, &v);
        udev_reg0x14 = v;

        printk(KERN_INFO MOD_NAME ": Saved values of regs 0x10 (0x%08x) and 0x14 (0x%08x)\n", udev_reg0x10, udev_reg0x14);
     }
  }

#if 0
  if (1) {
     u32 v;
     pci_read_config_dword(udev_pdev, 0x10, &v);

     if ((v & 0xF0000000) == 0) {
        printk(KERN_INFO MOD_NAME "::init: Config space [0x%02x] : 0x%08x, FPGA was probably reloaded\n", 0x10, v);
        
        if (1) {
           printk(KERN_INFO MOD_NAME ": Restoring PCI Config space\n");
           pci_write_config_dword(udev_pdev, 0x04, 0x00100006);
           pci_write_config_dword(udev_pdev, 0x10, 0xd000000C);
        }
     }
  }
#endif

  printk(KERN_INFO MOD_NAME ": driver init done\n");

  return 0;
}

void cleanup_module()
{
  printk(KERN_INFO MOD_NAME ": cleanup_module\n");

#ifdef HAVE_INTR
  //plx9080_intr_disable();

  if (udev_irq > 0)
    {
      disable_irq(udev_irq);
      free_irq(udev_irq, udev_pdev);
    }
#endif

  unregister_chrdev(MOD_MAJOR, MOD_NAME);

  if (udev_regs)
    iounmap(udev_regs);

  release_mem_region(pci_resource_start(udev_pdev,0), pci_resource_len(udev_pdev,0));

  printk(KERN_INFO MOD_NAME ": driver removed\n");
}

/*
 * the initialisation routine.
 * Return: 0 if okey, -Exxx if error.
 */
int init_module(void)
{
  printk(KERN_INFO MOD_NAME ": Linux driver by K.Olchanski/TRIUMF\n");
  printk(KERN_INFO MOD_NAME ": driver compiled " __DATE__ " " __TIME__ "\n");

  {
    int n=0;
    struct pci_dev *pdev = NULL;
    while ((pdev = pci_find_device(0x1172, 0x0004, pdev)))
      {
	int err = 0;

	printk(KERN_INFO MOD_NAME ": Found PCI device pdev 0x%p\n", pdev);

	n++;
	if (n > 1)
	  {
	    printk(KERN_ERR MOD_NAME ": Cannot handle more than one device, please contact author");
	    return -ENODEV;
	  }

	err = init(pdev);
	if (err)
	  return err;
      }

    if (n == 0)
      {
	printk(KERN_ERR MOD_NAME ": no devices found.\n");
	return -ENODEV;
      }
  }

  return 0;
}

/* end file */
